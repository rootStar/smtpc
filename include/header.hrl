
-define(CHILD(Id, Module, Type, Args), {
    Id,
    {Module, start_link, [Args]},
    permanent,
    5000,
    Type,
    [Module]
}).

-define(CHILD(Module, Type, Args), {
    Module,
    {Module, start_link, Args},
    permanent,
    5000,
    Type,
    [Module]
}).

-define(CHILD(Module, Type), {
    Module,
    {Module, start_link, []},
    permanent,
    5000,
    Type,
    [Module]
}).

-define(TIMEOUT, 1200000).

-define(MAIL_FROM, <<"MAIL FROM: ">>).

-define(RCPT_TO, <<"RCPT TO: ">>).
