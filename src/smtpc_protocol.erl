%%% Copyright 2009 Andrew Thompson <andrew@hijacked.us>. All rights reserved.
%%%
%%% Redistribution and use in source and binary forms, with or without
%%% modification, are permitted provided that the following conditions are met:
%%%
%%%   1. Redistributions of source code must retain the above copyright notice,
%%%      this list of conditions and the following disclaimer.
%%%   2. Redistributions in binary form must reproduce the above copyright
%%%      notice, this list of conditions and the following disclaimer in the
%%%      documentation and/or other materials provided with the distribution.
%%%
%%% THIS SOFTWARE IS PROVIDED BY THE FREEBSD PROJECT ``AS IS'' AND ANY EXPRESS OR
%%% IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
%%% MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
%%% EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
%%% INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
%%% (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
%%% LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
%%% ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
%%% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
%%% SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

%% @doc A simple SMTP client used for sending mail - assumes relaying via a
%% smarthost.

-module(smtpc_protocol).

-include("header.hrl").

-define(AUTH_PREFERENCE, [
	"CRAM-MD5",
	"LOGIN",
	"PLAIN"
]).

-export([
	try_EHLO/2,
	try_STARTTLS/4,
	do_STARTTLS/2,
	try_MAIL_FROM/3,
	try_RCPT_TO/3,
	try_DATA/3,
	try_RSET/1,
	try_AUTH/3,
	do_QUIT/1,
	try_connect/2
]).

%% ===================================================================
%% API Function Definitions
%% ===================================================================

try_connect(Host, Options) when is_binary(Host) ->
	try_connect(binary_to_list(Host), Options);
try_connect(Host, Options) ->
    AddSockOpts = case proplists:get_value(sockopts, Options) of
		undefined -> [];
		Other -> Other
	end,
    SockOpts = [binary, {packet, line}, {keepalive, true}, {active, false} | AddSockOpts],
    Proto = case proplists:get_value(ssl, Options) of
		true ->
			ssl;
		_ ->
			tcp
	end,
    Port = case proplists:get_value(port, Options) of
		undefined when Proto =:= ssl ->
			465;
		OPort when is_integer(OPort) ->
			OPort;
		_ ->
			25
	end,
    case smtpc_socket:connect(Proto, Host, Port, SockOpts, 5000) of
        {ok, Socket} ->
            case smtpc_util:read_possible_multiline_reply(Socket) of
                {ok, <<"220", Banner/binary>>} ->
                    {ok, Socket, Host, Banner};
                {ok, <<"4", _Rest/binary>> = Msg} ->
                    do_QUIT(Socket),
                    throw({temporary_failure, Msg});
                {ok, Msg} ->
                    do_QUIT(Socket),
                    throw({permanent_failure, Msg})
            end;
        {error, Reason} ->
            throw({network_failure, {error, Reason}})
    end.


-spec try_MAIL_FROM(From :: string() | binary(), Socket :: smtpc_socket:socket(), Extensions :: list()) -> true.
try_MAIL_FROM(From, Socket, _Extensions) ->
	smtpc_socket:send(Socket, smtpc_util:make_MAIL_FROM(From)),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"250", _Rest/binary>>} ->
			true;
		{ok, <<"4", _Rest/binary>> = Msg} ->
			do_QUIT(Socket),
			throw({temporary_failure, Msg});
		{ok, Msg} ->
			do_QUIT(Socket),
			throw({permanent_failure, Msg})
	end.

-spec try_RCPT_TO(Tos :: [binary() | string()], Socket :: smtpc_socket:socket(), Extensions :: list()) -> true.
try_RCPT_TO(To, Socket, _Extensions) ->
	smtpc_socket:send(Socket, smtpc_util:make_RCPT_TO(To)),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"250", _Rest/binary>>} ->
			true;
		{ok, <<"251", _Rest/binary>>} ->
			true;
		{ok, <<"4", _Rest/binary>> = Msg} ->
			do_QUIT(Socket),
			throw({temporary_failure, Msg});
		{ok, Msg} ->
			do_QUIT(Socket),
			throw({permanent_failure, Msg})
	end.

-spec try_DATA(Body :: binary() | function(), Socket :: smtpc_socket:socket(), Extensions :: list()) -> binary().
try_DATA(Body, Socket, Extensions) when is_function(Body) ->
    try_DATA(Body(), Socket, Extensions);
try_DATA(Body, Socket, _Extensions) ->
	smtpc_socket:send(Socket, "DATA\r\n"),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"354", _Rest/binary>>} ->
			EscapedBody = re:replace(Body, <<"^\\\.">>, <<"..">>, [global, unicode, multiline, {return, binary}]),
			smtpc_socket:send(Socket, [EscapedBody, "\r\n.\r\n"]),
			case smtpc_util:read_possible_multiline_reply(Socket) of
				{ok, <<"250 ", Receipt/binary>>} ->
					Receipt;
				{ok, <<"4", _Rest2/binary>> = Msg} ->
					do_QUIT(Socket),
					throw({temporary_failure, Msg});
				{ok, Msg} ->
					do_QUIT(Socket),
					throw({permanent_failure, Msg})
			end;
		{ok, <<"4", _Rest/binary>> = Msg} ->
			do_QUIT(Socket),
			throw({temporary_failure, Msg});
		{ok, Msg} ->
			do_QUIT(Socket),
			throw({permanent_failure, Msg})
	end.

-spec try_RSET(Socket :: smtpc_socket:socket()) -> atom() | no_return().
try_RSET(Socket) ->
	smtpc_socket:send(Socket, "RSET\r\n"),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"250 ", _Receipt/binary>>} ->
			true;
		{ok, <<"4", _Rest2/binary>> = Msg} ->
			do_QUIT(Socket),
			throw({temporary_failure, Msg});
		{ok, Msg} ->
			do_QUIT(Socket),
			throw({permanent_failure, Msg})
	end.

-spec try_AUTH(Socket :: smtpc_socket:socket(), Options :: list(), AuthTypes :: [string()]) -> boolean().
try_AUTH(Socket, Options, []) ->
	case proplists:get_value(auth, Options) of
		always ->
			do_QUIT(Socket),
			erlang:throw({missing_requirement, auth});
		_ ->
			false
	end;
try_AUTH(Socket, Options, undefined) ->
	case proplists:get_value(auth, Options) of
		always ->
			do_QUIT(Socket),
			erlang:throw({missing_requirement, auth});
		_ ->
			false
	end;
try_AUTH(Socket, Options, AuthTypes) ->
	case proplists:is_defined(username, Options) and
		proplists:is_defined(password, Options) and
		(proplists:get_value(auth, Options) =/= never) of
		false ->
			case proplists:get_value(auth, Options) of
				always ->
					do_QUIT(Socket),
					erlang:throw({missing_requirement, auth});
				_ ->
					false
			end;
		true ->

			Username = to_string(proplists:get_value(username, Options)),
			Password = to_string(proplists:get_value(password, Options)),
			Types = re:split(AuthTypes, " ", [{return, list}, trim]),
			case do_AUTH(Socket, Username, Password, Types) of
				false ->
					case proplists:get_value(auth, Options) of
						always ->
							do_QUIT(Socket),
							erlang:throw({permanent_failure, auth_failed});
						_ ->
							false
					end;
				true ->
					true
			end
	end.

to_string(String) when is_list(String)   -> String;
to_string(Binary) when is_binary(Binary) -> binary_to_list(Binary).

-spec do_AUTH(Socket :: smtpc_socket:socket(), Username :: string(), Password :: string(), Types :: [string()]) -> boolean().
do_AUTH(Socket, Username, Password, Types) ->
	FixedTypes = [string:to_upper(X) || X <- Types],
	AllowedTypes = [X  || X <- ?AUTH_PREFERENCE, lists:member(X, FixedTypes)],
	do_AUTH_each(Socket, Username, Password, AllowedTypes).

-spec do_AUTH_each(Socket :: smtpc_socket:socket(), Username :: string() | binary(), Password :: string() | binary(), AuthTypes :: [string()]) -> boolean().
do_AUTH_each(_Socket, _Username, _Password, []) ->
	false;
do_AUTH_each(Socket, Username, Password, ["CRAM-MD5" | Tail]) ->
	smtpc_socket:send(Socket, "AUTH CRAM-MD5\r\n"),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"334 ", Rest/binary>>} ->
			Seed64 = smtpc_util:bin_strip(smtpc_util:bin_strip(Rest, right, $\n), right, $\r),
			Seed = base64:decode_to_string(Seed64),
			Digest = smtpc_util:compute_cram_digest(Password, Seed),
			String = base64:encode(list_to_binary([Username, " ", Digest])),
			smtpc_socket:send(Socket, [String, "\r\n"]),
			case smtpc_util:read_possible_multiline_reply(Socket) of
				{ok, <<"235", _Rest/binary>>} ->
					true;
				{ok, _Msg} ->
					do_AUTH_each(Socket, Username, Password, Tail)
			end;
		{ok, _Something} ->
			do_AUTH_each(Socket, Username, Password, Tail)
	end;
do_AUTH_each(Socket, Username, Password, ["LOGIN" | Tail]) ->
	smtpc_socket:send(Socket, "AUTH LOGIN\r\n"),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		%% base64 Username: or username:
		{ok, Prompt} when Prompt == <<"334 VXNlcm5hbWU6\r\n">>; Prompt == <<"334 dXNlcm5hbWU6\r\n">> ->
			U = base64:encode(Username),
			smtpc_socket:send(Socket, [U,"\r\n"]),
			case smtpc_util:read_possible_multiline_reply(Socket) of
				%% base64 Password: or password:
				{ok, Prompt2} when Prompt2 == <<"334 UGFzc3dvcmQ6\r\n">>; Prompt2 == <<"334 cGFzc3dvcmQ6\r\n">> ->
					P = base64:encode(Password),
					smtpc_socket:send(Socket, [P,"\r\n"]),
					case smtpc_util:read_possible_multiline_reply(Socket) of
						{ok, <<"235 ", _Rest/binary>>} ->
							true;
						{ok, _Msg} ->
							do_AUTH_each(Socket, Username, Password, Tail)
					end;
				{ok, _Msg2} ->
					do_AUTH_each(Socket, Username, Password, Tail)
			end;
		{ok, _Something} ->
			do_AUTH_each(Socket, Username, Password, Tail)
	end;
do_AUTH_each(Socket, Username, Password, ["PLAIN" | Tail]) ->
	AuthString = base64:encode("\0"++Username++"\0"++Password),
	smtpc_socket:send(Socket, ["AUTH PLAIN ", AuthString, "\r\n"]),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"235", _Rest/binary>>} ->
			true;
		_Else ->
			% TODO do we need to bother trying the multi-step PLAIN?
			do_AUTH_each(Socket, Username, Password, Tail)
	end;
do_AUTH_each(Socket, Username, Password, [_Type | Tail]) ->
	do_AUTH_each(Socket, Username, Password, Tail).

-spec try_EHLO(Host :: string(), Socket :: smtpc_socket:socket()) -> {ok, list()}.
try_EHLO(Host, Socket) ->
	ok = smtpc_socket:send(Socket, ["EHLO ", Host, "\r\n"]),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"500", _Rest/binary>>} ->
			% Unrecognized command, fall back to HELO
			try_HELO(Host, Socket);
		{ok, <<"4", _Rest/binary>> = Msg} ->
			do_QUIT(Socket),
			throw({temporary_failure, Msg});
		{ok, Reply} ->
			{ok, smtpc_util:parse_extensions(Reply)}
	end.

-spec try_HELO(Host :: string(), Socket :: smtpc_socket:socket()) -> {ok, list()}.
try_HELO(Host, Socket) ->
	ok = smtpc_socket:send(Socket, ["HELO ", Host, "\r\n"]),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"250", _Rest/binary>>} ->
			{ok, []};
		{ok, <<"4", _Rest/binary>> = Msg} ->
			do_QUIT(Socket),
			throw({temporary_failure, Msg});
		{ok, Msg} ->
			do_QUIT(Socket),
			throw({permanent_failure, Msg})
	end.

% check if we should try to do TLS
-spec try_STARTTLS(Host :: string(), Socket :: smtpc_socket:socket(), Options :: list(), Extensions :: list()) -> {smtpc_socket:socket(), list()}.
try_STARTTLS(Host, Socket, Options, Extensions) ->
	case {proplists:get_value(tls, Options),
			proplists:get_value(<<"STARTTLS">>, Extensions)} of
		{Atom, true} when Atom =:= always; Atom =:= if_available ->
			case {do_STARTTLS(Host, Socket), Atom} of
				{false, always} ->
					do_QUIT(Socket),
					erlang:throw({temporary_failure, tls_failed});
				{false, if_available} ->
					{Socket, Extensions};
				{{S, E}, _} ->
					{S, E}
			end;
		{always, _} ->
			do_QUIT(Socket),
			erlang:throw({missing_requirement, tls});
		_ ->
			{Socket, Extensions}
	end.

%% attempt to upgrade socket to TLS
-spec do_STARTTLS(Host :: string(), Socket :: smtpc_socket:socket()) -> {smtpc_socket:socket(), list()} | false.
do_STARTTLS(Host, Socket) ->
	smtpc_socket:send(Socket, "STARTTLS\r\n"),
	case smtpc_util:read_possible_multiline_reply(Socket) of
		{ok, <<"220", _Rest/binary>>} ->
			case catch smtpc_socket:to_ssl_client(Socket, [], 5000) of
				{ok, NewSocket} ->
					%NewSocket;
					{ok, Extensions} = try_EHLO(Host, NewSocket),
					{NewSocket, Extensions};
				{'EXIT', Reason} ->
					do_QUIT(Socket),
					error_logger:error_msg("Error in ssl upgrade: ~p.~n", [Reason]),
					erlang:throw({temporary_failure, tls_failed});
				_Else ->
					false
			end;
		{ok, <<"4", _Rest/binary>> = Msg} ->
			do_QUIT(Socket),
			erlang:throw({temporary_failure, Msg});
		{ok, Msg} ->
			do_QUIT(Socket),
			erlang:throw({permanent_failure, Msg})
	end.

-spec do_QUIT(Socket :: smtpc_socket:socket()) -> ok | {error, atom()}.
do_QUIT(Socket) ->
	smtpc_socket:send(Socket, "QUIT\r\n"),
	smtpc_socket:close(Socket).
