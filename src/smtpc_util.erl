-module(smtpc_util).


-include("header.hrl").

-export([
    make_MAIL_FROM/1,
    make_RCPT_TO/1,
    make_DATA/1,
    guess_FQDN/0,
    get_registered_name/1,
    get_hosts_by_mx/2,
    mail_body_form/4,
    mail_body_form/5,
    compute_cram_digest/2,
    bin_strip/1,
    bin_strip/2,
    bin_strip/3,
    bin_substr/2,
    bin_substr/3,
    bin_strchr/2,
    bin_to_upper/1,
    read_possible_multiline_reply/1,
    parse_extensions/1
]).

-type bin_or_str() :: string() | binary().
-type from() :: {bin_or_str(), bin_or_str()} | bin_or_str().
-type to() :: {bin_or_str(), bin_or_str()} | bin_or_str() | [bin_or_str()].
-type message() :: nonempty_string() | binary().

-export_type([bin_or_str/0, from/0, to/0, message/0]).

-spec make_MAIL_FROM(From :: string() | binary()) -> iolist().
make_MAIL_FROM(From) when is_binary(From) ->
	make_MAIL_FROM(binary_to_list(From));
make_MAIL_FROM("<" ++ _ = From) ->
	[?MAIL_FROM, From, "\r\n"];
make_MAIL_FROM(From) ->
	make_MAIL_FROM("<"++From++">").


-spec make_RCPT_TO(Tos :: [binary() | string()]) -> iolist().
make_RCPT_TO([]) ->
	true;
make_RCPT_TO([To | Tail]) when is_binary(To) ->
	make_RCPT_TO([binary_to_list(To) | Tail]);
make_RCPT_TO(["<" ++ _ = To | _Tail]) ->
	[?RCPT_TO, To, "\r\n"];
make_RCPT_TO([To | Tail]) ->
	make_RCPT_TO(["<"++To++">" | Tail]).

make_DATA(Data) ->
    [
        "DATA\r\n",
        re:replace(Data, <<"^\\\.">>, <<"..">>, [global, unicode, multiline, {return, binary}]),
        "\r\n.\r\n"
    ].

get_registered_name(Pid) ->
    case proplists:get_value(registered_name, erlang:process_info(Pid)) of
        undefined ->
            Pid;
        Name ->
            Name
    end.

get_hosts_by_mx(RelayDomain, true) ->
    [{0, RelayDomain}];
get_hosts_by_mx(RelayDomain, _) ->
    case lists:keyfind(nameserver, 1, inet_db:get_rc()) of
        false ->
            % we got no nameservers configured, suck in resolv.conf
            inet_config:do_load_resolv(os:type(), longnames);
        _ ->
            ok
    end,
    MXRecords = case inet_res:lookup(RelayDomain, in, mx) of
        [] ->
            [];
        Result ->
            lists:sort(fun({Pref, _Name}, {Pref2, _Name2}) -> Pref =< Pref2 end, Result)
    end,
    case MXRecords of
        [] ->
            [{0, RelayDomain}]; % maybe we're supposed to relay to a host directly
        _ ->
            MXRecords
    end.

-spec guess_FQDN() -> string().
guess_FQDN() ->
    {ok, Hostname} = inet:gethostname(),
    {ok, Hostent} = inet:gethostbyname(Hostname),
    {hostent, FQDN, _Aliases, inet, _, _Addresses} = Hostent,
    FQDN.

-spec compute_cram_digest(Key :: string(), Data :: string()) -> binary().
compute_cram_digest(Key, Data) ->
	Bin = crypto:hmac(md5, Key, Data),
	list_to_binary([io_lib:format("~2.16.0b", [X]) || <<X>> <= Bin]).


-spec bin_strchr(Bin :: binary(), C :: char()) -> non_neg_integer().
bin_strchr(Bin, C) when is_binary(Bin) ->
    case binary:match(Bin, <<C>>) of
        {Index, _Length} ->
            Index + 1;
        nomatch ->
            0
    end.

-spec bin_to_upper(Bin :: binary()) -> binary().
bin_to_upper(Bin) ->
	bin_to_upper(Bin, <<>>).

bin_to_upper(<<>>, Acc) ->
	Acc;
bin_to_upper(<<H, T/binary>>, Acc) when H >= $a, H =< $z ->
	H2 = H - 32,
	bin_to_upper(T, <<Acc/binary, H2>>);
bin_to_upper(<<H, T/binary>>, Acc) ->
	bin_to_upper(T, <<Acc/binary, H>>).

-spec bin_strip(Bin :: binary()) -> binary().
bin_strip(Bin) ->
	bin_strip(Bin, both, $\s).

-spec bin_strip(Bin :: binary(), Dir :: 'left' | 'right' | 'both') -> binary().
bin_strip(Bin, Dir) ->
	bin_strip(Bin, Dir, $\s).

-spec bin_strip(Bin :: binary(), Dir :: 'left' | 'right' | 'both', C :: non_neg_integer()) -> binary().
bin_strip(<<>>, _, _) ->
	<<>>;
bin_strip(Bin, both, C) ->
	bin_strip(bin_strip(Bin, left, C), right, C);
bin_strip(<<C, _Rest/binary>> = Bin, left, C) ->
	bin_strip(bin_substr(Bin, 2), left, C);
bin_strip(Bin, left, _C) ->
	Bin;
bin_strip(Bin, right, C) ->
	L = byte_size(Bin),
	case binary:at(Bin, L-1) of
		C ->
			bin_strip(binary:part(Bin, 0, L-1), right, C);
		_ ->
			Bin
    end.

-spec bin_substr(Bin :: binary(), Start :: pos_integer() | neg_integer()) -> binary().
bin_substr(<<>>, _) ->
	<<>>;
bin_substr(Bin, Start) when Start > 0 ->
	{_, B2} = split_binary(Bin, Start-1),
	B2;
bin_substr(Bin, Start) when Start < 0 ->
	Size = byte_size(Bin),
	{_, B2} = split_binary(Bin, Size+Start),
	B2.

-spec bin_substr(Bin :: binary(), Start :: pos_integer() | neg_integer(), Length :: pos_integer()) -> binary().
bin_substr(<<>>, _, _) ->
	<<>>;
bin_substr(Bin, Start, Length) when Start > 0 ->
	{_, B2} = split_binary(Bin, Start-1),
	{B3, _} = split_binary(B2, Length),
	B3;
bin_substr(Bin, Start, Length) when Start < 0 ->
	Size = byte_size(Bin),
	{_, B2} = split_binary(Bin, Size+Start),
	{B3, _} = split_binary(B2, Length),
	B3.

-spec mail_body_form(From :: from(), To :: to(), Subject :: bin_or_str(), Message :: message()) -> iolist().
mail_body_form(From, To, Subject, Message) ->
    mail_body_form(From, To, Subject, Message, []).

-spec mail_body_form(From :: from(), To :: to(), Subject :: bin_or_str(), Message :: message(), Headers :: list()) -> iolist().
mail_body_form(From, To, Subject, Message, Headers) ->
    [
        "MIME-Version: 1.0\r\n",
        format_subject(Subject),
        format_from(From),
        format_to(To),
        Headers,
        "Content-Type: text/html; charset=utf-8\r\n",
        "Content-Transfer-Encoding: base64\r\n",
        "\r\n",
        base64:encode(unicode:characters_to_binary(Message))
    ].

-spec format_subject(Subject :: string()) -> iolist();
                    (Subject :: binary()) -> iolist().
format_subject(Subject) when is_binary(Subject) ->
    [
        "Subject: =?utf-8?B?",
        base64:encode(Subject),
        "?=\r\n"
    ];
format_subject(Subject) ->
    [
        "Subject: =?utf-8?B?",
        base64:encode(unicode:characters_to_binary(Subject)),
        "?=\r\n"
    ].

-spec format_from(From :: from()) -> iolist().
format_from({From, Sender}) when is_binary(From) ->
    [
        "From: =?utf-8?B?",
        base64:encode(From),
        "?= <",
        Sender,
        ">\r\n"
    ];
format_from({From, Sender}) ->
    [
        "From: =?utf-8?B?",
        base64:encode(unicode:characters_to_binary(From)),
        "?= <",
        Sender,
        ">\r\n"
    ];
format_from(From) when is_binary(From) ->
    [
        "From: =?utf-8?B?",
        base64:encode(From),
        "?=\r\n"
    ];
format_from(From) ->
    [
        "From: =?utf-8?B?",
        base64:encode(unicode:characters_to_binary(From)),
        "?=\r\n"
    ].

-spec format_to(To :: to()) -> iolist().
format_to({To, Reciever}) when is_binary(To) ->
    [
        "To: =?utf-8?B?",
        base64:encode(To),
        "?= <",
        Reciever,
        ">\r\n"
    ];
format_to({To, Reciever}) ->
    [
        "To: =?utf-8?B?",
        base64:encode(unicode:characters_to_binary(To)),
        "?= <",
        Reciever,
        ">\r\n"
    ];
format_to([FirstReciever|_] = Recievers) when is_list(Recievers) andalso is_list(FirstReciever) ->
    Formatted = lists:map(fun(Reciever) when is_list(Reciever) -> "<" ++ Reciever ++ ">";
        (Reciever) when is_binary(Reciever) -> "<" ++ binary_to_list(Reciever) ++ ">"
    end, Recievers),
    [
        "To: ",
        string:join(Formatted, ", "),
        "\r\n"
    ];
format_to(To) when is_binary(To) ->
    [
        "To: =?utf-8?B?",
        base64:encode(To),
        "?=\r\n"
    ];
format_to(To) ->
    [
        "To: =?utf-8?B?",
        base64:encode(unicode:characters_to_binary(To)),
        "?=\r\n"
    ].


%% read a multiline reply (eg. EHLO reply)
-spec read_possible_multiline_reply(Socket :: smtpc_socket:socket()) -> {ok, binary()}.
read_possible_multiline_reply(Socket) ->
	case smtpc_socket:recv(Socket, 0, ?TIMEOUT) of
		{ok, Packet} ->
			case bin_substr(Packet, 4, 1) of
				<<"-">> ->
					Code = bin_substr(Packet, 1, 3),
					read_multiline_reply(Socket, Code, [Packet]);
				<<" ">> ->
					{ok, Packet}
			end;
		Error ->
			throw({network_failure, Error})
	end.

-spec read_multiline_reply(Socket :: smtpc_socket:socket(), Code :: binary(), Acc :: [binary()]) -> {ok, binary()}.
read_multiline_reply(Socket, Code, Acc) ->
	case smtpc_socket:recv(Socket, 0, ?TIMEOUT) of
		{ok, Packet} ->
			case {bin_substr(Packet, 1, 3), bin_substr(Packet, 4, 1)} of
				{Code, <<" ">>} ->
					{ok, list_to_binary(lists:reverse([Packet | Acc]))};
				{Code, <<"-">>} ->
					read_multiline_reply(Socket, Code, [Packet | Acc]);
				_ ->
					smtpc_protocol:do_QUIT(Socket),
					throw({unexpected_response, lists:reverse([Packet | Acc])})
			end;
		Error ->
			throw({network_failure, Error})
	end.

-spec parse_extensions(Reply :: binary()) -> [{binary(), binary()}].
parse_extensions(Reply) ->
	[_ | Reply2] = re:split(Reply, "\r\n", [{return, binary}, trim]),
	[
		begin
				Body = bin_substr(Entry, 5),
				case re:split(Body, " ",  [{return, binary}, trim, {parts, 2}]) of
					[Verb, Parameters] ->
						{bin_to_upper(Verb), Parameters};
					[Body] ->
						case bin_strchr(Body, $=) of
							0 ->
								{bin_to_upper(Body), true};
							_ ->
								[]
						end
				end
		end  || Entry <- Reply2].
