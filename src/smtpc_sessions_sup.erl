%%% @author   Nick Roschin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roschin.
%%% @doc

-module(smtpc_sessions_sup).

-behaviour(supervisor).

-include("header.hrl").
%% API
-export([start_link/1]).

%% Supervisor callbacks
-export([init/1]).

%% ===================================================================
%% API functions
%% ===================================================================

start_link(Config) ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, Config).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init(Config) ->
    SupFlags = {
        simple_one_for_one,
        proplists:get_value(sup_intensity, Config, 5),
        proplists:get_value(sup_period, Config, 10)
    },
    {ok, { SupFlags, [?CHILD(smtpc_session, worker, [])] } }.
