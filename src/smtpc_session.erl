%%% @author   Nick Roshin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roshin.
%%% @doc

-module(smtpc_session).

-behaviour(gen_server).

-define(SERVER, ?MODULE).

-include("header.hrl").

-author('Nick Roshin <deneb808@gmail.com>').


%% ===================================================================
%% API Function Exports
%% ===================================================================

-export([
    start_link/1,
    start/1,
    start/2,
    stop/1,
    stop/2,
    send/4,
    start_queue/1,
    start_queue/2,
    stop_queue/1,
    add_to_queue/4
]).

%% ===================================================================
%% gen_server Function Exports
%% ===================================================================

-export([init/1,
    handle_call/3,
    handle_cast/2,
    terminate/2,
    handle_info/2,
    code_change/3]).

-define(EMPTY_BUFFER, <<>>).

-record(state, {
    options,
    socket,
    extensions,
    buffer = ?EMPTY_BUFFER,
    qctimeout = false,
    till_stop = false,
    queued = [],
    results = []
}).

-define(ONE_SECOND, 1000).

-define(QUEUE_TIMEOUT, 250).


-define(DEFAULT_OPTIONS, [
	{ssl, false}, % true / false
	{tls, if_available}, % always, never, if_available
	{auth, if_available}, % always, never, if_available
    {hostname, smtpc_util:guess_FQDN()},
    {no_mx_lookups, false},
    {buffer_limit, false}, % buffer limit for queue send
    {limit_per_sec, false}, % 1 MB/sec limit
    {stop_passive_queue, 5000}, % stop eque after 5000ms
	{retries, 1} % how many retries per smtp host on temporary failure
]).

-type email() :: {string() | binary(), [string() | binary(), ...], string() | binary() | function()}.

%% ===================================================================
%% API Function Definitions
%% ===================================================================

start_link(Options) ->
    gen_server:start_link(?MODULE, Options, []).

%% -------------------------------------------------------------------
%% API for default smtpc_sessions_sup
%% -------------------------------------------------------------------

start(Options) ->
    start(smtpc_sessions_sup, Options).

stop(Id) ->
    stop(smtpc_sessions_sup, Id).

%% -------------------------------------------------------------------
%% API for custom supervisor
%% -------------------------------------------------------------------

start(SupRef, Options) ->
    supervisor:start_child(SupRef, [Options]).

stop(SupRef, Id) ->
    supervisor:terminate_child(SupRef, Id).

%% -------------------------------------------------------------------
%% API for sending messages
%% -------------------------------------------------------------------
send(ServerRef, From, [First| _]= To, Message) when is_list(To) andalso is_list(First) ->
    gen_server:call(ServerRef, {send_message, From, To, Message});
send(ServerRef, From, To, Message) ->
    gen_server:call(ServerRef, {send_message, From, [To], Message}).

start_queue(ServerRef) ->
    start_queue(ServerRef, ?QUEUE_TIMEOUT).

start_queue(_ServerRef, QueueCheckTimeout) when QueueCheckTimeout > ?ONE_SECOND ->
    {error, one_second_limit_exceed};
start_queue(ServerRef, QueueCheckTimeout) ->
    gen_server:call(ServerRef, {start_queue, QueueCheckTimeout}).

stop_queue(ServerRef) ->
    gen_server:call(ServerRef, stop_queue).

add_to_queue(ServerRef, From, [First| _]= To, Message) when is_list(To) andalso is_list(First) ->
    gen_server:call(ServerRef, {add_to_queue, From, To, Message});
add_to_queue(ServerRef, From, To, Message) ->
    gen_server:call(ServerRef, {add_to_queue, From, [To], Message}).

%% ===================================================================
%% gen_server Function Definitions
%% ===================================================================

init(Options) ->
    process_flag(trap_exit, true),
    NewOptions = lists:ukeymerge(1, lists:sort(Options), lists:sort(?DEFAULT_OPTIONS)),
    {Socket, Extensions} = open_session(NewOptions),
    gen_event:notify(smtpc_dispatcher, {started, self()}),
    {ok, #state{ options = NewOptions, socket = Socket, extensions = Extensions }}.

handle_call({send_message, From, To, Body}, _From, State) ->
    {reply, send_message(From, To, Body, State), State};

handle_call({start_queue, Timeout}, _From, #state{socket = Socket} = State) ->
    smtpc_socket:setopts(Socket, [{active, true}]),
    erlang:send_after(Timeout, self(), send_queued),
    gen_event:notify(smtpc_dispatcher, {start_queue, self(), Timeout}),
    {reply, ok, State#state{ qctimeout = Timeout }};

handle_call(stop_queue, _From, State) ->
    {reply, ok, stop_queue_internal(State)};


handle_call({add_to_queue, From, To, Body}, _From, #state{options = Opts, queued = Queued,  buffer = Buff} = State) ->
    Ref = make_ref(),
    BufferLimit = proplists:get_value(buffer_limit, Opts),
    Email = [
        smtpc_util:make_MAIL_FROM(From),
        smtpc_util:make_RCPT_TO(To),
        smtpc_util:make_DATA(Body)
    ],
    case BufferLimit >= iolist_size(Email) + iolist_size(Buff) of
        false ->
            {reply, {error, buffer_limit_exceed}, State};
        true ->
            {reply, Ref, State#state{ queued = Queued ++ [Ref], till_stop = false, buffer = iolist_to_binary([Buff, Email]) }}
    end.


handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(send_queued, #state{qctimeout = false} = State) ->
    {noreply, State};

handle_info(send_queued, #state{queued = []} = State) ->
    #state{
        qctimeout = Timeout,
        till_stop = TillStop,
        options = Options
    } = State,
    gen_event:notify(smtpc_dispatcher, {queue_is_empty, self()}),
    NewState = case TillStop of
        false->
            erlang:send_after(Timeout, self(), send_queued),
            State#state{till_stop = proplists:get_value(stop_passive_queue, Options)};
        Value when Value > 0 ->
            erlang:send_after(Timeout, self(), send_queued),
            State#state{till_stop = TillStop - Timeout };
        _Value ->
            stop_queue_internal(State)
    end,
    {noreply, NewState};

handle_info(send_queued, #state{buffer = <<>>} = State) ->
    error_logger:error_msg("Buffer is <<>>~n", []),
    {noreply, State};

handle_info(send_queued, State) ->
    #state{
        qctimeout = Timeout,
        options = Options,
        socket = Socket,
        buffer = Buffer
    } = State,
    erlang:send_after(Timeout, self(), send_queued),
    BufferSize = byte_size(Buffer),
    NewState = case proplists:get_value(limit_per_sec, Options) of
        false ->
            smtpc_socket:send(Socket, Buffer),
            gen_event:notify(smtpc_dispatcher, {send_queued, self(), Timeout, BufferSize, BufferSize}),
            State#state{ buffer = ?EMPTY_BUFFER };
        Val ->
            Limit = round(Val / (?ONE_SECOND/Timeout)),
            gen_event:notify(smtpc_dispatcher, {send_queued, self(), Timeout, Limit, BufferSize}),
            case BufferSize >= Limit of
                true ->
                    smtpc_socket:send(Socket, binary_part(Buffer, {0, Limit})),
                    State#state{ buffer = binary_part(Buffer, {Limit, byte_size(Buffer) - Limit}) };
                false ->
                    smtpc_socket:send(Socket, Buffer),
                    State#state{ buffer = ?EMPTY_BUFFER }
            end
    end,
    {noreply, NewState};

handle_info({_CType, {_SType, {_, Port, _, _}, _Pid}, Message}, #state{ queued = [] } = State) ->
    error_logger:error_msg("Get message ~p on socket port ~p with empty queued~n", [Message, Port]),
    {noreply, State};

handle_info({_Type, _Socket, Message}, #state{ queued = [Ref | Tail] } = State) ->
    #state{
        results = Results,
        socket  = Socket,
        buffer  = Buffer
    } = State,
    NewState = case Message of
        <<"250 2.0.0 Ok: queued as ", _Uid/binary>> = Success->
            State#state{
                queued = Tail,
                results = [{Ref, {ok, trim_answer(Success)}} | Results]
            };
        <<"250 2.0.0", _Rest/binary>> ->
            State;
        <<"250 2.1.0", _Rest/binary>> ->
            State;
        <<"250 2.1.5", _Rest/binary>> ->
            State;
        <<"354", _Rest/binary>> ->
            State;
        Unexpected ->
            PartialState = State#state{
                queued  = Tail,
                results = [{Ref, {error, trim_answer(Unexpected)}} | Results]
            },
            case reset_buffer(Socket, Buffer) of
                {ok, NewBuffer} ->
                    PartialState#state{ buffer  = NewBuffer };
                {error, NewBuffer} ->
                    ReState = reopen_socket(Socket, PartialState),
                    ReState#state{ buffer  = NewBuffer }
            end
    end,
    {noreply, NewState};

handle_info({ssl_closed, Socket}, State) ->
    {noreply, reopen_socket(Socket, State)};

handle_info({tcp_closed, Socket}, State) ->
    {noreply, reopen_socket(Socket, State)};

handle_info(Info, State) ->
    error_logger:error_msg("Unhandled ~p~n", [Info]),
    {noreply, State}.

terminate(Reason, State) ->
    gen_event:notify(smtpc_dispatcher, {stopped, self(), Reason}),
    smtpc_protocol:do_QUIT(State#state.socket).

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ===================================================================
%% Internal Function Definitions
%% ===================================================================
stop_queue_internal(#state{ socket = Socket } = State) ->
    smtpc_socket:setopts(Socket, [{active, false}]),
    gen_event:notify(smtpc_dispatcher, {stop_queue, self()}),
    State#state{ qctimeout = false, till_stop = false }.

reset_buffer(Socket, Buffer) ->
    NewBuffer = case binary:match(Buffer, ?MAIL_FROM) of
        nomatch ->
            ?EMPTY_BUFFER;
        {Start, _Len} ->
            binary:part(Buffer, Start, byte_size(Buffer) - Start)
    end,
    smtpc_socket:setopts(Socket, [{active, false}]),
    try smtpc_protocol:try_RSET(Socket) of
        true ->
            smtpc_socket:setopts(Socket, [{active, true}]),
            {ok, NewBuffer}
    catch
        Trow ->
            error_logger:error_msg("Get ~p on reset buffer~n", [Trow]),
            {error, NewBuffer}
    end.

trim_answer(Binary) ->
    binary:replace(Binary, <<"\r\n">>, ?EMPTY_BUFFER).

reopen_socket(Socket, #state{options = Options} = State) ->
    error_logger:error_msg("Socket ~p is closed~n", [Socket]),
    {NewSocket, Extensions} = open_session(Options),
    smtpc_socket:setopts(NewSocket, [{active, true}]),
    State#state{socket = NewSocket, extensions = Extensions}.

send_message(From, To, Body, State) ->
    BodySize = iolist_size([From, To, Body]),
    gen_event:notify(smtpc_dispatcher, {before_send, self(), {From, To, BodySize}}),
    Result = try_sending_it({From, To, Body}, State),
    gen_event:notify(smtpc_dispatcher, {after_send, self(), {From, To, BodySize}, Result}),
    Result.

-spec try_sending_it(Email :: email(), State :: #state{}) -> tuple() | no_return().
try_sending_it(Email, State) ->
	try do_sending_it(Email, State) of
        Result ->
            {ok, Result}
    catch
        {network_failure,{error,closed}} = Disconnect ->
            exit(Disconnect);
        Throw ->
            {error, Throw}
    end.

-spec do_sending_it(Email :: email(), State :: #state{}) -> binary().
do_sending_it({From, To, Body}, #state{socket = Sock, extensions = Exts} = _State) ->
	smtpc_protocol:try_MAIL_FROM(From, Sock, Exts),
	smtpc_protocol:try_RCPT_TO(To, Sock, Exts),
	smtpc_protocol:try_DATA(Body, Sock, Exts).

open_session(Options) ->
    RelayHost = proplists:get_value(relay, Options),
    NoMxLookups = proplists:get_value(no_mx_lookups, Options),
	Hosts = smtpc_util:get_hosts_by_mx(RelayHost, NoMxLookups),
    gen_event:notify(smtpc_dispatcher, {mx_lookup, self(), Hosts}),
    case try_smtp_sessions(Hosts, Options, []) of
        {error, Type, _Data} ->
            exit({Type, _Data});
        {Socket, Extensions} ->
            {Socket, Extensions}
    end.

-spec try_smtp_sessions(Hosts :: [{non_neg_integer(), string()}, ...], Options :: list(), RetryList :: list()) -> binary() | {'error', any(), any()}.
try_smtp_sessions([{_Distance, Host} | _Tail] = Hosts, Options, RetryList) ->
	try do_smtp_session(Host, Options) of
		Result ->
            Result
	catch
		Throw ->
			handle_smtp_throw(Throw, Hosts, Options, RetryList)
	end.

do_smtp_session(Host, Options) ->
    {ok, Socket, _Host, Banner} = smtpc_protocol:try_connect(Host, Options),
    gen_event:notify(smtpc_dispatcher, {banner, self(), Banner}),

    {ok, Extensions} = smtpc_protocol:try_EHLO(Host, Socket),
    {Socket2, Extensions2} = smtpc_protocol:try_STARTTLS(Host, Socket, Options, Extensions),
    gen_event:notify(smtpc_dispatcher, {extensions, self(), Extensions2}),

    Authed = smtpc_protocol:try_AUTH(Socket2, Options, proplists:get_value(<<"AUTH">>, Extensions2)),
    gen_event:notify(smtpc_dispatcher, {authed, self(), Authed}),
    {Socket2, Extensions2}.

handle_smtp_throw({permanent_failure, Message}, [{_Distance, Host} | _Tail], _Options, _RetryList) ->
	{error, no_more_hosts, {permanent_failure, Host, Message}};
handle_smtp_throw({temporary_failure, tls_failed}, [{_Distance, Host} | _Tail] = Hosts, Options, RetryList) ->
	case proplists:get_value(tls, Options) of
		if_available ->
			NoTLSOptions = [{tls,never} | proplists:delete(tls, Options)],
			try do_smtp_session(Host, NoTLSOptions) of
				Res -> Res
			catch
				FailMsg ->
					handle_smtp_throw(FailMsg, Hosts, Options, RetryList)
			end;
		_ ->
			try_next_host({temporary_failure, tls_failed}, Hosts, Options, RetryList)
	end;
handle_smtp_throw(FailMsg, Hosts, Options, RetryList) ->
	try_next_host(FailMsg, Hosts, Options, RetryList).

try_next_host({FailureType, Message}, [{_Distance, Host} | _Tail] = Hosts, Options, RetryList) ->
	Retries = proplists:get_value(retries, Options),
	RetryCount = proplists:get_value(Host, RetryList),
	case fetch_next_host(Retries, RetryCount, Hosts, RetryList) of
		{[], _NewRetryList} ->
			{error, retries_exceeded, {FailureType, Host, Message}};
		{NewHosts, NewRetryList} ->
			try_smtp_sessions(NewHosts, Options, NewRetryList)
	end.

fetch_next_host(Retries, RetryCount, [{_Distance, Host} | Tail], RetryList) when is_integer(RetryCount), RetryCount >= Retries ->
	{Tail, lists:keydelete(Host, 1, RetryList)};
fetch_next_host(_Retries, RetryCount, [{Distance, Host} | Tail], RetryList) when is_integer(RetryCount) ->
	{Tail ++ [{Distance, Host}], lists:keydelete(Host, 1, RetryList) ++ [{Host, RetryCount + 1}]};
fetch_next_host(0, _RetryCount, [{_Distance, Host} | Tail], RetryList) ->
	{Tail, lists:keydelete(Host, 1, RetryList)};
fetch_next_host(_Retries, _RetryCount, [{Distance, Host} | Tail], RetryList) ->
	{Tail ++ [{Distance, Host}], lists:keydelete(Host, 1, RetryList) ++ [{Host, 1}]}.
