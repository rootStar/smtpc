%%% @author   Nick Roschin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roschin.
%%% @doc

-module(smtpc_events_logger).

-behaviour(gen_event).

-author('Nick Roschin <deneb808@gmail.com>').

%% ===================================================================
%% API Function Exports
%% ===================================================================


%% ===================================================================
%% gen_event Function Exports
%% ===================================================================

-export([init/1,
    handle_event/2,
    handle_call/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-record(state, {}).

%% ===================================================================
%% API Function Definitions
%% ===================================================================


%% ===================================================================
%% gen_event Function Definitions
%% ===================================================================

init([]) ->
    {ok, #state{}}.

handle_event({mx_lookup, Id, Hosts}, State) ->
    error_logger:info_msg("[~p] Mx lookup hosts ~p~n", [Id, Hosts]),
    {ok, State};
handle_event({banner, Id, Banner}, State) ->
    error_logger:info_msg("[~p] Get banner ~p~n", [Id, Banner]),
    {ok, State};
handle_event({extensions, Id, Extensions}, State) ->
    error_logger:info_msg("[~p] Available extensions ~p~n", [Id, Extensions]),
    {ok, State};
handle_event({authed, Id, Authed}, State) ->
    error_logger:info_msg("[~p] Authed is ~p~n", [Id, Authed]),
    {ok, State};

handle_event({before_send, Id, {From, To, BodySize}}, State) ->
    error_logger:info_msg("[~p] Start sending (~pb) ~p => ~p~n", [Id, BodySize, From, To]),
    {ok, State};
handle_event({after_send, Id, {From, To, BodySize}, {ok, Result}}, State) ->
    error_logger:info_msg("[~p] Email (~pb) ~p => ~p sended with ~p~n", [Id, BodySize, From, To, Result]),
    {ok, State};
handle_event({after_send, Id, {From, To, BodySize}, {error, Result}}, State) ->
    error_logger:error_msg("[~p] Email (~pb) ~p =x= ~p not sended with ~p~n", [Id, BodySize, From, To, Result]),
    {ok, State};

handle_event({start_queue, Id, Timeout}, State) ->
    error_logger:info_msg("[~p] Queue is started with ~p timeout~n", [Id, Timeout]),
    {ok, State};
handle_event({stop_queue, Id}, State) ->
    error_logger:info_msg("[~p] Queue is stopped~n", [Id]),
    {ok, State};
handle_event({send_queued, Id, Timeout, Sended, Total}, State) ->
    error_logger:info_msg("[~p] Sended ~p/~p with timeout ~pms~n", [Id, Sended, Total, Timeout]),
    {ok, State};
handle_event({queue_is_empty, Id}, State) ->
    error_logger:warning_msg("[~p] Queue is empty~n", [Id]),
    {ok, State};

handle_event({started, Id}, State) ->
    error_logger:info_msg("[~p] Connection is started~n", [Id]),
    {ok, State};
handle_event({stopped, Id, shutdown}, State) ->
    error_logger:info_msg("[~p] Closed normal~n", [Id]),
    {ok, State};
handle_event({stopped, Id, Reason}, State) ->
    error_logger:error_msg("[~p] Terminate with ~p~n", [Id, Reason]),
    {ok, State};
handle_event(Event, State) ->
    error_logger:warning_msg("Unhandled event ~p~n", [Event]),
    {ok, State}.

handle_call(_Request, State) ->
    Reply = ok,
    {ok, Reply, State}.

handle_info(_Info, State) ->
    {ok, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ===================================================================
%% Internal Function Definitions
%% ===================================================================
