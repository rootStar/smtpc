-module(smtpc).

-include("header.hrl").

%% ===================================================================
%% API Function Exports
%% ===================================================================

-export([send/5, test/1]).

-define(mail_opts, [
    {relay, "postfix.firefly.red"},
    {port, 9267},
    {auth, never},
    {username, "user1"},
    {password, "12345"},
    {limit_per_sec,  300 * 1024}
]).

-define(min_mail_size, 100 * 1024).

-define(max_mail_size, 1024*1024).

%%====================================================================
%% API
%%====================================================================

random(Min, Max) ->
    random:seed(erlang:monotonic_time()),
    round(random:uniform() * (Max - Min) + Min).

random_string(Len) ->
    Chrs = list_to_tuple("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),
    ChrsSize = size(Chrs),
    F = fun(_, R) -> [element(random:uniform(ChrsSize), Chrs) | R] end,
    lists:foldl(F, "", lists:seq(1, Len)).

test(Count) ->
    {ok, Pid} = smtpc_session:start(?mail_opts),
    smtpc_session:start_queue(Pid),
    lists:foreach(fun(N) ->
        Body = apply(smtpc_util, mail_body_form, [
            "user1@example.com",
            "blackhole@localhost",
            "Test message",
            random_string(random(?min_mail_size, ?max_mail_size))
        ]),
        smtpc_session:add_to_queue(Pid, "user"++ integer_to_list(N) ++"@example.com", "blackhole@localhost", Body)
    end, lists:seq(1, Count)).
    % smtpc_session:stop(Pid).


-spec send(From :: smtpc_util:from(),
        To :: smtpc_util:to(),
        Subject :: smtpc_util:bin_or_str(),
        Message :: smtpc_util:message(),
        Options :: maybe_improper_list()) -> binary().
send({_FromName, FromEmail} = From, {_ToName, ToEmail} = To, Subject, Message, Options) ->
    {ok, Pid} = smtpc_session:start(Options),
    Mail = smtpc_util:mail_body_form(From, To, Subject, Message),
    Result = smtpc_session:send(Pid, FromEmail, [ToEmail], Mail),
    ok = smtpc_session:stop(Pid),
    Result;
send({_FromName, FromEmail} = From, [FirstTo|_] = ToEmails, Subject, Message, Options) when is_list(ToEmails) andalso is_list(FirstTo) ->
    {ok, Pid} = smtpc_session:start(Options),
    Mail = smtpc_util:mail_body_form(From, ToEmails, Subject, Message),
    Result = smtpc_session:send(Pid, FromEmail, ToEmails, Mail),
    ok = smtpc_session:stop(Pid),
    Result;
send(From, [FirstTo|_] = ToEmails, Subject, Message, Options) when is_list(ToEmails) andalso is_list(FirstTo) ->
    {ok, Pid} = smtpc_session:start(Options),
    Mail = smtpc_util:mail_body_form(From, ToEmails, Subject, Message),
    Result = smtpc_session:send(Pid, From, ToEmails, Mail),
    ok = smtpc_session:stop(Pid),
    Result;
send({_FromName, FromEmail} = From, To, Subject, Message, Options) ->
    {ok, Pid} = smtpc_session:start(Options),
    Mail = smtpc_util:mail_body_form(From, {To, To}, Subject, Message),
    Result = smtpc_session:send(Pid, FromEmail, [To], Mail),
    ok = smtpc_session:stop(Pid),
    Result;
send(From, To, Subject, Message, Options) ->
    {ok, Pid} = smtpc_session:start(Options),
    Mail = smtpc_util:mail_body_form(From, {To, To}, Subject, Message),
    Result = smtpc_session:send(Pid, From, [To], Mail),
    ok = smtpc_session:stop(Pid),
    Result.
