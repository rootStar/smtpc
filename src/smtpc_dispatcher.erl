%%% @author   Nick Roshin <deneb808@gmail.com>
%%% @copyright  2015  Nick Roshin.
%%% @doc

-module(smtpc_dispatcher).

-behaviour(gen_event).

-author('Nick Roshin <deneb808@gmail.com>').

%% ===================================================================
%% API Function Exports
%% ===================================================================

-export([start_link/1, add_handler/2]).

%% ===================================================================
%% gen_event Function Exports
%% ===================================================================

-export([init/1,
    handle_event/2,
    handle_call/2,
    handle_info/2,
    terminate/2,
    code_change/3]).

-record(state, {}).

%% ===================================================================
%% API Function Definitions
%% ===================================================================

start_link(Listeners) ->
    Result = gen_event:start_link({local, ?MODULE}),
    add_handlers(Listeners),
    Result.

add_handlers([]) -> ok;
add_handlers([{Handler, Args} | Tail]) ->
    add_handler(Handler, Args),
    add_handlers(Tail).

add_handler(Handler, Args) ->
    gen_event:add_handler(?MODULE, Handler, Args).

%% ===================================================================
%% gen_event Function Definitions
%% ===================================================================

init([]) ->
    {ok, #state{}}.

handle_event(_Event, State) ->
    {ok, State}.

handle_call(_Request, State) ->
    {ok, ok, State}.

handle_info(_Info, State) ->
    {ok, State}.

terminate(_Reason, _State) ->
    ok.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ===================================================================
%% Internal Function Definitions
%% ===================================================================
