%%%-------------------------------------------------------------------
%% @doc smtpc top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module('smtpc_sup').

-behaviour(supervisor).

-include("header.hrl").

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Config = application:get_all_env(smtpc),
    Listeners = proplists:get_value(events_listeners, Config, []),
    {ok, { {one_for_one, 5, 10}, [
            ?CHILD(smtpc_dispatcher, worker, [Listeners]),
            ?CHILD(smtpc_sessions_sup, supervisor, [Config])
        ]
    }}.

%%====================================================================
%% Internal functions
%%====================================================================
